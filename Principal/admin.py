from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from .models import Cliente
from .models import Image
from .models import Notificacion
from .models import * 
 #Register your models here.
admin.site.register(Cliente)
admin.site.register(Image)
admin.site.register(Factura)
admin.site.register(DetalleFactura)
admin.site.register(Reserva)
admin.site.register(DetalleReserva)
class AdminNotificacion(admin.ModelAdmin):
	list_display = ['nombresInvitado', 'fechaEnvio', 'celular', 'correo']
	search_fields = ['nombresInvitado']
	class Meta:
		model = Notificacion
admin.site.register(Notificacion, AdminNotificacion)


