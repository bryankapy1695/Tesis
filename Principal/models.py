from __future__ import unicode_literals
from django.conf import settings
from django.core.validators import MaxValueValidator
from django.db import models
from django.contrib.auth.models import User

#Modelo para la clase Imagen

class Image(models.Model):
	image = models.ImageField(upload_to='slide')
	nombreImg = models.CharField(max_length=50, unique=True)
	def __str__(seft):
		return seft.nombreImg

#Modelo para la clase Notificaciones

class Notificacion(models.Model):
	fechaEnvio = models.DateField( ("Date"), auto_now_add = True)
	nombresInvitado = models.CharField(max_length=50) 
	celular = models.CharField(max_length=10, unique=True)
	correo = models.EmailField()
	mensaje = models.TextField()
	def __str__(seft):
		return seft.nombresInvitado


#Modelo para la clase Usuario

class Cliente(User):
	nombreCompleto = models.CharField(max_length=70)
	CI_Ruc = models.CharField(max_length=13, unique=True)
	telefono = models.CharField(max_length=10)
	direccion = models.CharField(max_length=200)
	def __str__(seft):
		return seft.nombreCompleto

class Reserva(models.Model):
	"""docstring for Reserva"""
	cliente = models.ForeignKey('Cliente')
	fecha_reserva = models.DateField(auto_now_add = True, auto_now = False)
	fecha_entrada = models.DateField()
	fecha_salida = models.DateField()
	dias = models.IntegerField()
	estado = models.IntegerField()
	abono = models.BooleanField() 
	total = models.DecimalField(max_digits=8, decimal_places=2)
	

	def __unicode__(self):	
		return "%s" % self.abono

class DetalleReserva(models.Model):
	"""docstring for Detalle"""

	cantidad = models.IntegerField()
	descripcion = models.CharField(max_length = 30)
	precio_uni = models.DecimalField(max_digits=5, decimal_places=2)
	precio_total = models.DecimalField(max_digits=7, decimal_places=2)
	reserva = models.ForeignKey('Reserva')
	habitacion = models.ForeignKey('Habitacion')
	id_tipohabitacion = models.IntegerField()


	def __unicode__(self):
		return "%s" % self.cantidad

class Factura(models.Model):
	"""docstring for Factura"""

	fecha = models.DateTimeField(auto_now_add=True, blank=True)### OJO
	id_reserva = models.IntegerField()
	cliente = models.ForeignKey('Cliente') 
	detalle = models.ForeignKey('DetalleFactura')
	subtotal = models.DecimalField(max_digits=7, decimal_places=2)
	iva =  models.DecimalField(max_digits=6, decimal_places=2)
	descuento = models.DecimalField(max_digits=6, decimal_places=2)
	total = models.DecimalField(max_digits=8, decimal_places=2)
	numero_factura = models.CharField(max_length = 15)
	estado_factura = models.BooleanField()

	def __unicode__(self):
		return "%s" % self.numero_factura

class DetalleFactura(models.Model):
	"""docstring for DetalleFactura"""
	cantidad = models.IntegerField()
	precio_uni = models.DecimalField(max_digits=5, decimal_places=2)
	precio_total = models.DecimalField(max_digits=7, decimal_places=2)
	habitacion = models.ForeignKey('Habitacion')
	servicio = models.ForeignKey('Servicio')

	def __init__(self, arg):
		return "%s" % self.cantidad
		
class TipoServicio(models.Model):
	
	nombre_tipo = models.CharField(max_length=50, unique=True)
	def __str__(seft):
		return seft.nombre_tipo
		

class Servicio(models.Model):
 	"""docstring for Servicio"""
 	nombre = models.CharField(max_length = 40)
 	precio = models.DecimalField(max_digits=7, decimal_places=2)
 	estado = models.BooleanField()
 	tiposervicio = models.ForeignKey('TipoServicio')

 	def __unicode__(self):
 		return "%s" % self.nombre
 		 		

class Habitacion(models.Model):
	"""docstring for Habitacion"""
	class Meta:
		 verbose_name_plural = "Habitaciones"
	numero_habitacion= models.IntegerField()
	piso_habitacion = models.IntegerField()
	estado_habitacion = models.CharField(max_length= 50)
	tipohabitacion = models.ForeignKey('TipoHabitacion')

	def __unicode__(self):
		return __str__(self.numero_habitacion)

class TipoHabitacion(models.Model):
	"""docstring for TipoHabitacion"""
	class Meta:
		 verbose_name_plural = "Tipo Habitaciones"
	nombre_tipo = models.CharField(max_length = 100)
	precio_tipo = models.DecimalField(max_digits=6, decimal_places=2)

	def __unicode__(self):
		return "%s" % self.nombre_tipo	


