from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render , redirect
from django.template import RequestContext
from django.template import Context
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from django.views import generic
from django.views.generic import View 
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from .forms import ClienteForm
from .models import Cliente
from .forms import ImageForm, UsuarioForm
from .models import Image
from .forms import NotificacionForm
from .models import Notificacion
from .forms import TipoHabitacionForm
from .models import TipoHabitacion
from .forms import HabitacionForm
from .models import Habitacion
from .forms import TipoServicioForm
from .models import TipoServicio
from .forms import ServicioForm
from .models import Servicio, Factura

#**************************************
#FUNCIONES PARA VISTA PRINCIPAL
#**************************************

def mostrar_inicio(request):
	imagenes = Image.objects.all()
	context = {
		'index': imagenes,
		}
	return render(request, 'Principal/index.html',context)

def quienes_somos(request):
	return render(request, 'Principal/quienes_somos.html',{})

def habitaciones(request):
	return render(request, 'Principal/habitaciones.html',{})

def habitaciones2(request):
	return render(request, 'Principal/habitaciones2.html',{})

def habitacion_detalle(request):
	return render(request, 'Principal/habitacion_detalle.html',{})

def mostrar_servicios(request):
	return render(request, 'Principal/servicios.html',{})	

def mostrar_galeria(request):
	return render(request, 'Principal/galeria.html',{})	

#**************************************
#FUNCIONES PARA VISTA ADMINISTRACION
#**************************************

def bienvenida(request):
	return render(request, 'Principal/bienvenida.html',{})

def factura(request):
	return render(request, 'Principal/factura.html',{})

def lista_factura(request):
	return render(request, 'Principal/consultar_factura.html',{})

def lista_reservaciones(request):
	return render(request, 'Principal/consultar_reservacion.html',{})

def mostrar_administracion(request):
	return render(request, 'Principal/administracion/index.html',{})

def mostrar_administracion_forms(request):
	return render(request, 'Principal/administracion/forms.html',{})

def mostrar_administracion_table(request):
	return render(request, 'Principal/administracion/table.html',{})

def mostrar_administracion_ui(request):
	return render(request, 'Principal/administracion/ui.html',{})

def mostrar_administracion_blank(request):
	return render(request, 'Principal/administracion/blank.html',{})

def control_habitaciones(request):
	return render(request, 'Principal/control_habitaciones.html',{})

def mostrar_usuario(request):
	return render(request, 'Principal/usuario.html',{})

def crear_usuario(request):
	return render(request, 'Principal/crear_usuario.html',{})

def tipo_servicio(request):
	return render(request, 'Principal/tipo_servicio.html',{})

def servicio(request):
	return render(request, 'Principal/servicio.html',{})

def tipo_habitacion(request):
	return render(request, 'Principal/tipo_habitacion.html',{})

def habitacion(request):
	return render(request, 'Principal/habitacion.html',{})

def mostrar_cliente(request):
	cliente = Cliente.objects.all()
	context = {
		'cliente': cliente,
		}
	return render(request, 'Principal/cliente.html',context)

def mostrar_contactos(request):
	if request.method == "POST":
		form = NotificacionForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			notificaciones= form.save()
			return redirect('lista_contactos')
	else:
		form = NotificacionForm()

	return render(request, 'Principal/contactos.html',{'form':form})

def mostrar_notificaciones(request):
	notificacion = Notificacion.objects.all()
	context = {
		'notificacion': notificacion,
		}
	return render(request, 'Principal/lista_notificaciones.html',context)

def reservacion1(request):
	return render(request, 'Principal/reservacion1.html',{})

def reservacion2(request):
	return render(request, 'Principal/reservacion2.html',{})

def reservacion3(request):
	return render(request, 'Principal/reservacion3.html',{})

def reservacion4(request):
	return render(request, 'Principal/reservacion4.html',{})


def lista_image(request):
	imagenes = Image.objects.all()
	context = {
		'index': imagenes,
		}
	return render(request, 'Principal/index.html',context)


def add_usuario(request):
	if request.method=="POST":
		form_usuario=ClienteForm(request.POST)
		if form_usuario.is_valid:
			try:
				user = form_usuario.save(commit=False)
				username = form_usuario.cleaned_data['username']
				password = form_usuario.cleaned_data['password']
				user.set_password(password)
				user.save()

				user = authenticate(username=username, password=password)
				if user.is_active:
					login(request, user)
					return redirect('login')
					form_usuario.save()
					return render(request, 'Principal/registration_form.html',{})
			except:
				return render(request, self.template_name, {'form_usuario': form_usuario})
		else:
			form_usuario=ClienteForm()
	return render(request, 'Principal/registration_form.html',{})


def vistaAdministrador(request):
	return render(request, 'Principal/vistaAdministrador.html',{})

def vistaUsuario(request):
	return render(request, 'Principal/vistaUsuario.html',{})

def mostrar_lista(request):
	imagenes = Image.objects.all()
	context = {
		'imagenes': imagenes,
		}
	return render(request, 'Principal/mostrar_lista.html',context)

def crear_cliente(request):
	if request.method=="POST":
		form_usuario=ClienteForm(request.POST)
		if form_usuario.is_valid:
			try:
				user = form_usuario.save(commit=False)
				username = form_usuario.cleaned_data['username']
				password = form_usuario.cleaned_data['password']
				user.set_password(password)
				user.save()

				user = authenticate(username=username, password=password)
				if user.is_active:
					return redirect('cliente')
					form_usuario.save()
					return render(request, 'Principal/crear_cliente.html',{})
			except:
				return render(request, self.template_name, {'form_usuario': form_usuario})
		else:
			form_usuario=ClienteForm()
	return render(request, 'Principal/crear_cliente.html',{})

def editar_cliente(request, id_usuario):
	#En esta lina se realiza la consulta de la base de datos mediante un queryset
	obj_usuario = Cliente.objects.get(id=id_usuario)
	if request.method=="POST":
		form = ClienteForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			nombreCompleto = form.cleaned_data['nombreCompleto']
			CI_Ruc = form.cleaned_data['CI_Ruc']
			email = form.cleaned_data['email']
			telefono = form.cleaned_data['telefono']
			direccion = form.cleaned_data['direccion']
			obj_usuario.username = username
			obj_usuario.password = password
			obj_usuario.nombreCompleto = nombreCompleto
			obj_usuario.CI_Ruc = CI_Ruc
			obj_usuario.email = email
			obj_usuario.telefono = telefono
			obj_usuario.direccion = direccion
			#Guardamos el modelo editado con el .save()
			obj_usuario.save()
			return redirect('cliente')
			pass
		pass
	if request.method=="GET":
		form = ClienteForm(initial={
			'username':obj_usuario.username,
			'password':obj_usuario.password,
			'nombreCompleto':obj_usuario.nombreCompleto,
			'CI_Ruc':obj_usuario.CI_Ruc,
			'email':obj_usuario.email,
			'telefono':obj_usuario.telefono,
			'direccion':obj_usuario.direccion,
			})
		contexto = {'form':form, 'cliente':obj_usuario}
		pass
	return render(request, 'Principal/editar_cliente.html', contexto)

def eliminar_cliente(request, id_usuario):
	obj_usuario = Cliente.objects.get(id=id_usuario)
	if request.method=="GET":
		obj_usuario.delete()
		return redirect('cliente')

	return render(request, 'Principal/eliminar_cliente.html', {'usuario':obj_usuario})

########################################################################################
#                              CLIENTE
########################################################################################

##LISTA DE CLIENTES
        
class ClienteList(ListView):
	model = Cliente
	template_name = 'Principal/cliente.html'

##EDITAR CLIENTE

class ClienteUpdate(UpdateView):
	model = Cliente
	form_class = ClienteForm
	template_name = 'Principal/editar_cliente.html'
	success_url = reverse_lazy('cliente')

##ELIMINAR CLIENTE

class ClienteDelete(DeleteView):
	model = Cliente
	template_name = 'Principal/eliminar_cliente.html'
	success_url = reverse_lazy('cliente')

########################################################################################
#                              IMAGE SLIDE
########################################################################################

##EDITAR Imagen Slide

class ImageUpdate(UpdateView):
	model = Image
	form_class = ImageForm
	template_name = 'Principal/editar_image.html'
	success_url = reverse_lazy('imagenes')

########################################################################################
#                              USUARIO
########################################################################################

##CREAR USUARIO

class CrearUsuario(CreateView):
	model = User
	template_name = 'Principal/crear_usuario.html'
	form_class = UsuarioForm
	success_url = reverse_lazy('usuario')

##LISTA DE USUARIOS
        
class UsuarioList(ListView):
	model = User
	template_name = 'Principal/usuario.html'

##EDITAR USUARIO

class UsuarioUpdate(UpdateView):
	model = User
	form_class = UsuarioForm
	template_name = 'Principal/editar_usuario.html'
	success_url = reverse_lazy('usuario')

##ELIMINAR USUARIO

class UsuarioDelete(DeleteView):
	model = User
	template_name = 'Principal/eliminar_usuario.html'
	success_url = reverse_lazy('usuario')

########################################################################################
#                              NOTIFICACIONES
########################################################################################

##LISTA DE NOTIFICACIONES
        
class NotificacionList(ListView):
	model = Notificacion
	template_name = 'Principal/lista_notificaciones.html'

##vER NOTIFICACIONES

class VerNotificiones(UpdateView):
	model = Notificacion
	form_class = NotificacionForm
	template_name = 'Principal/ver_notificacion.html'
	success_url = reverse_lazy('lista_notificaciones')



########################################################################################
#                              TIPO HABITACION
########################################################################################

##CREAR TIPO HABITACION

class CrearTipoHabitacion(CreateView):
	model = TipoHabitacion
	template_name = 'Principal/crear_tipo_habitacion.html'
	form_class = TipoHabitacionForm
	success_url = reverse_lazy('tipo_habitacion')

##LISTA DE TIPO HABITACION
        
class TipoHabitacionList(ListView):
	model = TipoHabitacion
	template_name = 'Principal/tipo_habitacion.html'

##EDITAR TIPO HABITACION

class TipoHabitacionUpdate(UpdateView):
	model = TipoHabitacion
	form_class = TipoHabitacionForm
	template_name = 'Principal/editar_tipo_habitacion.html'
	success_url = reverse_lazy('tipo_habitacion')

##ELIMINAR TIPO HABITACION

class TipoHabitacionDelete(DeleteView):
	model = TipoHabitacion
	template_name = 'Principal/eliminar_tipo_habitacion.html'
	success_url = reverse_lazy('tipo_habitacion')	

########################################################################################
#                              HABITACION
########################################################################################

##CREAR HABITACION

class CrearHabitacion(CreateView):
	model = Habitacion
	template_name = 'Principal/crear_habitacion.html'
	form_class = HabitacionForm
	success_url = reverse_lazy('habitacion')

##LISTA HABITACION
        
class HabitacionList(ListView):
	model = Habitacion
	template_name = 'Principal/habitacion.html'

##EDITAR HABITACION

class HabitacionUpdate(UpdateView):
	model = Habitacion
	form_class = HabitacionForm
	template_name = 'Principal/editar_habitacion.html'
	success_url = reverse_lazy('habitacion')

##ELIMINAR HABITACION

class HabitacionDelete(DeleteView):
	model = Habitacion
	template_name = 'Principal/eliminar_habitacion.html'
	success_url = reverse_lazy('habitacion')	


########################################################################################
#                              TIPO SERVICIO
########################################################################################

##CREAR TIPO SERVICIO 

class CrearTipoServicio(CreateView):
	model = TipoServicio
	template_name = 'Principal/crear_tipo_servicio.html'
	form_class = TipoServicioForm
	success_url = reverse_lazy('tipo_servicio')

##LISTA DE TIPO SERVICIO
        
class TipoServicioList(ListView):
	model = TipoServicio
	template_name = 'Principal/tipo_servicio.html'

##EDITAR TIPO SERVICIO

class TipoServicioUpdate(UpdateView):
	model = TipoServicio
	form_class = TipoServicioForm
	template_name = 'Principal/editar_tipo_servicio.html'
	success_url = reverse_lazy('tipo_servicio')

##ELIMINAR TIPO SERVICIO

class TipoServicioDelete(DeleteView):
	model = TipoServicio
	template_name = 'Principal/eliminar_tipo_servicio.html'
	success_url = reverse_lazy('tipo_servicio')	

########################################################################################
#                              SERVICIO
########################################################################################

##CREAR HABITACION

class CrearServicio(CreateView):
	model = Servicio
	template_name = 'Principal/crear_servicio.html'
	form_class = ServicioForm
	success_url = reverse_lazy('servicio')

##LISTA HABITACION
        
class ServicioList(ListView):
	model = Servicio
	template_name = 'Principal/servicio.html'

##EDITAR HABITACION

class ServicioUpdate(UpdateView):
	model = Servicio
	form_class = ServicioForm
	template_name = 'Principal/editar_servicio.html'
	success_url = reverse_lazy('servicio')

##ELIMINAR HABITACION

class ServicioDelete(DeleteView):
	model = Servicio
	template_name = 'Principal/eliminar_servicio.html'
	success_url = reverse_lazy('servicio')	

########################################################################################
#                              TIPO HABITACION PARA RESERVA
########################################################################################
		
##LISTA DE TIPO TIPO HABITACION
        
class ReservacionTipoHabitacionList(ListView):
	model = TipoHabitacion
	template_name = 'Principal/reservacion1.html'


########################################################################################
#                              SERVICIO  PARA FACTURA
########################################################################################
		
##LISTA DE TIPO TIPO HABITACION
        
class ServicioFacturaList(ListView):
	model = Servicio
	template_name = 'Principal/factura.html'
