from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import Cliente
from .models import Image 
from .models import Notificacion, TipoHabitacion, Habitacion, TipoServicio, Servicio


class ImageForm(forms.ModelForm):
	class Meta:
		model = Image
		fields = '__all__'

class NotificacionForm(forms.ModelForm):
	class Meta:
		model = Notificacion

		fields = [
		'nombresInvitado',
		'celular',
		'correo',
		'mensaje',

		]

		labels ={
		'nombresInvitado': 'Nombre Completo',
		'celular': 'Celular',
		'correo': 'Correo',
		'mensaje': 'Tu mensaje',
		}

		widgets = {
		'nombresInvitado': forms.TextInput(attrs={'class':'form-control'}),
		'celular': forms.TextInput(attrs={'class':'form-control'}),
		'correo': forms.TextInput(attrs={'class':'form-control'}),
		'mensaje': forms.Textarea(attrs={'cols': 80, 'rows': 20}),
		}
			
		
class ClienteForm(forms.ModelForm):
	
	class Meta:
		model = Cliente

		fields = [
		'username',
		'password',
		'nombreCompleto',
		'CI_Ruc',
		'email',
		'telefono',
		'direccion',

		]

		labels ={
		'username': 'Usuario',
		'password': 'Contrasena',
		'nombreCompleto': 'Nombres y Apellidos',
		'CI_Ruc': 'Cedula o RUC',
		'email': 'Correo',
		'telefono': 'Telefono',
		'direccion': 'Direccion',
		}

		widgets = {
		'username': forms.TextInput(attrs={'class':'form-control'}),
		'password': forms.PasswordInput(attrs={'class':'form-control'}),
		'nombreCompleto': forms.TextInput(attrs={'class':'form-control'}),
		'CI_Ruc': forms.TextInput(attrs={'class':'form-control'}),
		'email': forms.TextInput(attrs={'class':'form-control'}),
		'telefono': forms.TextInput(attrs={'class':'form-control'}),
		'direccion': forms.TextInput(attrs={'class':'form-control'}),
		}

class UsuarioForm(UserCreationForm):
	
	class Meta:
		model = User
		fields = [
		'username',
		'first_name',
		'last_name',
		'email',
		]

		labels ={
		'username': 'Nombre de Usuario',
		'first_name':'Nombres',
		'last_name':'Apellidos',
		'email':'Correo',
		}

class TipoHabitacionForm(forms.ModelForm):
	
	class Meta:
		model = TipoHabitacion
		fields = [
		'nombre_tipo',
		'precio_tipo',
		]

		labels ={
		'nombre_tipo':'Nombre Tipo',
		'precio_tipo':'Precio',
		}

class HabitacionForm(forms.ModelForm):
	
	class Meta:
		model = Habitacion
		fields = [
		'numero_habitacion',
		'piso_habitacion',
		'estado_habitacion',
		'tipohabitacion',
		]

		labels ={
		'numero_habitacion':'Numero de Habitacion',
		'piso_habitacion':'Piso de Habitacion',
		'estado_habitacion':'Estado',
		'tipohabitacion':'Tipo',
		}

class TipoServicioForm(forms.ModelForm):
	
	class Meta:
		model = TipoServicio
		fields = [
		'nombre_tipo',
		]

		labels ={
		'nombre_tipo':'Nombre Tipo',
		}

class ServicioForm(forms.ModelForm):
	
	class Meta:
		model = Servicio
		fields = [
		'nombre',
		'precio',
		'estado',
		'tiposervicio',
		]

		labels ={
		'nombre':'Nombre',
		'precio':'Precio',
		'estado':'Estado',
		'tiposervicio':'Tipo',
		}


