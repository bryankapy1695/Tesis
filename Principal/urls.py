from django.conf.urls import include,url
from . import views
from .views import ClienteList, ClienteUpdate, ImageUpdate, CrearUsuario, UsuarioList, UsuarioDelete, NotificacionList, VerNotificiones, UsuarioUpdate, ClienteDelete
from .views import CrearTipoHabitacion, TipoHabitacionList, TipoHabitacionUpdate, TipoHabitacionDelete, CrearHabitacion, HabitacionList, HabitacionUpdate, HabitacionDelete
from .views import CrearTipoServicio, TipoServicioList, TipoServicioUpdate, TipoServicioDelete, CrearServicio, ServicioList, ServicioUpdate, ServicioDelete, ReservacionTipoHabitacionList
from .views import *
from django.contrib.auth.views import login

urlpatterns = [
    url(r'^$', views.mostrar_inicio, name='index'),
    url(r'^quienes_somos/$', views.quienes_somos, name='quienes_somos'),
    url(r'^habitaciones/$', views.habitaciones, name='habitaciones'),
    url(r'^habitaciones/2/$', views.habitaciones2, name='habitaciones2'),
    url(r'^habitacion_detalle/$', views.habitacion_detalle, name='habitacion_detalle'),
    url(r'^servicios/$', views.mostrar_servicios, name='mostrar_servicios'),
    url(r'^galeria/$', views.mostrar_galeria, name='mostrar_galeria'),
    url(r'^contactos/$', views.mostrar_contactos, name='mostrar_contactos'),
    url(r'^notificaciones/lista/$', NotificacionList.as_view(), name='lista_notificacion'),
    url(r'^notificaciones/ver/(?P<pk>\d+)/$', VerNotificiones.as_view(), name='ver_notificacion'),
    url(r'^reservacion/1/$', ReservacionTipoHabitacionList.as_view(), name='reservacion'),
    url(r'^reservacion/2/$', views.reservacion2, name='reservacion2'),
    url(r'^reservacion/3/$', views.reservacion3, name='reservacion3'),
    url(r'^reservacion/4/$', views.reservacion4, name='reservacion4'),
    url(r'^$', views.lista_image, name='lista1'),
    url(r'^imagenes/editar/(?P<pk>\d+)/$', ImageUpdate.as_view(), name='editar_image'),
    url(r'^registrar/$', views.add_usuario, name='registrar'),
    url(r'^bienvenida/$', views.bienvenida, name='bienvenida'),
    url(r'^administracion/$', views.mostrar_administracion, name='administracion'),
    url(r'^administracion/forms/$', views.mostrar_administracion_forms, name='administracion_forms'),
    url(r'^administracion/table/$', views.mostrar_administracion_table, name='administracion_table'),
    url(r'^administracion/ui/$', views.mostrar_administracion_ui, name='administracion_ui'),
    url(r'^administracion/blank/$', views.mostrar_administracion_blank, name='administracion_blank'),
    url(r'^bienvenida/administrador/$', views.vistaAdministrador, name='administrador'),

    url(r'^bienvenida/usuario/$', UsuarioList.as_view(), name='usuario'),
    url(r'^bienvenida/usuario/crear/$', CrearUsuario.as_view(), name='crear_usuario'),
    url(r'^bienvenida/usuario/editar/(?P<pk>\d+)/$', UsuarioUpdate.as_view(), name='eliminar_usuario'),
    url(r'^bienvenida/usuario/eliminar/(?P<pk>\d+)/$', UsuarioDelete.as_view(), name='eliminar_usuario'),

    url(r'^bienvenida/administrador/slide/$', views.mostrar_lista, name='imagenes'),
    url(r'^factura/$', ServicioFacturaList.as_view(), name='factura'),
    url(r'^factura/consultar/$', views.lista_factura, name='consultar_factura'),
    url(r'^reservaciones/consultar/$', views.lista_reservaciones, name='consultar_reservaciones'),

    url(r'^bienvenida/cliente/$', ClienteList.as_view(), name='cliente'),
    url(r'^bienvenida/cliente/nuevo/$', views.crear_cliente, name='cliente_nuevo'),
    url(r'^bienvenida/cliente/editar/(?P<pk>\d+)/$', ClienteUpdate.as_view(), name='cliente_editar'),
    url(r'^bienvenida/cliente/eliminar/(?P<pk>\d+)/$', ClienteDelete.as_view(), name='eliminar_cliente'),

    url(r'^bienvenida/administracion/usuario/$', views.mostrar_usuario, name='usuario_administracion'),
    url(r'^notificacion/$', views.mostrar_notificaciones, name='notificacion'),
    url(r'^habitaciones/control/$', views.control_habitaciones, name='control_habitaciones'),

    url(r'^tipo/habitacion/$', TipoHabitacionList.as_view(), name='tipo_habitacion'),
    url(r'^tipo/habitacion/crear/$', CrearTipoHabitacion.as_view(), name='crear_tipo_habitacion'),
    url(r'^tipo/habitacion/editar/(?P<pk>\d+)/$', TipoHabitacionUpdate.as_view(), name='editar_tipo_habitacion'),
    url(r'^tipo/habitacion/eliminar/(?P<pk>\d+)/$', TipoHabitacionDelete.as_view(), name='eliminar_tipo_habitacion'),

    url(r'^habitacion/$', HabitacionList.as_view(), name='habitacion'),
    url(r'^habitacion/crear/$', CrearHabitacion.as_view(), name='crear_habitacion'),
    url(r'^habitacion/editar/(?P<pk>\d+)/$', HabitacionUpdate.as_view(), name='editar_habitacion'),
    url(r'^habitacion/eliminar/(?P<pk>\d+)/$', HabitacionDelete.as_view(), name='eliminar_habitacion'),

    url(r'^tipo/servicio/$', TipoServicioList.as_view(), name='tipo_servicio'),
    url(r'^tipo/servicio/crear/$', CrearTipoServicio.as_view(), name='crear_tipo_servicio'),
    url(r'^tipo/servicio/editar/(?P<pk>\d+)/$', TipoServicioUpdate.as_view(), name='editar_tipo_servicio'),
    url(r'^tipo/servicio/eliminar/(?P<pk>\d+)/$', TipoServicioDelete.as_view(), name='eliminar_tipo_servicio'),

    url(r'^servicio/$', ServicioList.as_view(), name='servicio'),
    url(r'^servicio/crear/$', CrearServicio.as_view(), name='crear_servicio'),
    url(r'^servicio/editar/(?P<pk>\d+)/$', ServicioUpdate.as_view(), name='editar_servicio'),
    url(r'^servicio/eliminar/(?P<pk>\d+)/$', ServicioDelete.as_view(), name='eliminar_servicio'),

    
]